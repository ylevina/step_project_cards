import { default as VisitDentist } from "./visitDentist.js";
import { default as VisitCardiologist } from "./visitCardiologist.js";
import { default as VisitTherapist } from "./visitTherapist.js";

export default function visitFactory(card) {
    try {
        if (card.doctor == "Dentist") return new VisitDentist(card.id, card.title, card.description, card.doctor, card.status, card.priority, card.patient, card.comments, card.lastVisitDate);
        if (card.doctor == "Therapist") return new VisitTherapist(card.id, card.title, card.description, card.doctor, card.status, card.priority, card.patient, card.comments, card.age);
        if (card.doctor == "Cardiologist") return new VisitCardiologist(card.id, card.title, card.description, card.doctor, card.status, card.priority, card.patient, card.comments, card.bp, card.bmi, card.deseases);

    } catch (e) {
        console.error('Error:', e);
    }
}
