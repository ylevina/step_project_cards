import { default as Visit } from "./visit.js";
import Text from "../components/text.js";

export default class VisitCardiologist extends Visit{
    constructor(id, title, description, doctor, status, priority, patient, comments, bp, bmi, deseases) {
        super(id, title, description, doctor, status, priority, patient, comments);
        this._bp = bp;
        this._bmi = bmi;
        this._deseases = deseases;     
    }

    bpProps = {
        type: "text",
        name: "bp",
        id: "bp",
        className: "bp"
    };

    bmiProps = {
        type: "text",
        name: "bmi",
        id: "bmi",
        className: "bmi"
    };

    deseasesProps = {
        type: "text",
        name: "deseases",
        id: "deseases",
        className: "deseases"
    };

    render() {
        const {
            bpProps,            
            bmiProps,        
            deseasesProps     
        } = this;

        const card = super.render();
        const customFields = card.querySelector('.card-custom');

        const bp = new Text(bpProps).render();
        bp.textContent = `Давление: ${this._bp}`;

        const bmi = new Text(bmiProps).render();
        bmi.textContent = `Индекс массы тела: ${this._bmi}`;

        const deseases = new Text(deseasesProps).render();
        deseases.textContent = `Болезни: ${this._deseases}`;
       
        customFields.prepend(deseases);
        customFields.prepend(bmi);
        customFields.prepend(bp);

        return card;
    }
};
