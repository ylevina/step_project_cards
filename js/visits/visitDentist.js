import { default as Visit } from "./visit.js";
import Text from "../components/text.js";

export default class VisitDentist extends Visit {
    constructor(id, title, description, doctor, status, priority, patient, comments, lastVisitDate) {
        super(id, title, description, doctor, status, priority, patient, comments);
        this._lastVisitDate = lastVisitDate;
    };

    lastVisitDateProps = {
        type: "text",
        name: "lastVisitDate",
        id: "lastVisitDate",
        className: "lastVisitDate"
    };

    render() {
        const {
            lastVisitDateProps     
        } = this;

        const card = super.render();
        const customFields = card.querySelector('.card-custom');      

        const lastVisitDate = new Text(lastVisitDateProps).render();
        lastVisitDate.textContent = `Последний визит: ${this._lastVisitDate}`;
       
        customFields.prepend(lastVisitDate);

        return card;
    }
}
