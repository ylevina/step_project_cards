import Visit from "./visit.js";
import Text from "../components/text.js";

export default class VisitTherapist extends Visit{
    constructor(id, title, description, doctor, status, priority, patient, comments, age) {
        super(id, title, description, doctor, status, priority, patient, comments);
        this._age = age;    
    }; 

    ageProps = {
        type: "text",
        name: "age",
        id: "age",
        className: "age"
    };

    render() {
        const {
            ageProps     
        } = this;

        const card = super.render();
        const customFields = card.querySelector('.card-custom');      

        const age = new Text(ageProps).render();
        age.textContent = `Возраст: ${this._age}`;
       
        customFields.prepend(age);

        return card;
    }
}