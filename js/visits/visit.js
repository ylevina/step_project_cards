import { deleteVisit } from "../fetch.js";
import { default as Select } from "../components/select.js";
import { EditVisitModal } from '../components/modal/modalEditVisit.js';
import Button from "../components/button.js";
import Text from "../components/text.js";

export default class Visit {
    constructor(id, title, description, doctor, status, priority, patient, comments) {
        this._id = id;
        this._title = title;
        this._description = description;
        this._doctor = doctor;
        this._status = status;
        this._priority = priority;
        this._patient = patient;
        this._comments = comments;
    }

    get id() {
        return this._id;
    }

    set id(value) {
        this._id = value;
    }

    doctorProps = {
        type: "text",
        name: "doctor",
        id: "doctorId",
        className: "doctor"
    };
    
    titleProps = {
        type: "text",
        name: "title",
        id: "title",
        className: "title"      
    };

    descriptionProps = {
        type: "text",
        name: "description",
        id: "description",
        className: "description"
    };

    priorityProps = {
        type: "text",
        name: "priority",
        id: "priority",
        className: "priority"
    };

    patientProps = {
        type: "text",
        name: "patient",
        className: "patient",
        id: 'patient'
    };

    commentsProps = {
        type: "text",
        name: "comments",
        id: "comments",
        className: "comments",
    };

    statusProps = {
        type: "hidden",
        value: "новый",
        name: "status",
        id: "status",
    };

    editProps = {
        options: [
            { text: 'Действия' },
            { value: "edit", text: 'Редактировать' },
            { value: 'delete', text: 'Удалить' },
        ],
        id: 'edit',
        className: "form-control"
    };

    showMoreProps = {
        textContent: "Показать больше",
        id: "showMore"
    };    

    handleShowMore(e, elem) {
        elem.classList.toggle("hide");
        if(elem.classList.contains("hide")) e.target.textContent = "Показать больше";
        else e.target.textContent = "Показать меньше";
    }

    render() {
        const {
            doctorProps,            
            titleProps,        
            descriptionProps,        
            priorityProps,        
            patientProps,        
            commentsProps,        
            statusProps,
            showMoreProps,
            editProps        
        } = this;

        const card = document.createElement('div');
        card.classList.add("card");

        const cardHeader = document.createElement('div');
        cardHeader.classList.add("card-header");

        const cardBody = document.createElement('div');
        cardBody.classList.add("card-body");

        const cardCustom = document.createElement('div');
        cardCustom.classList.add("card-custom");
        cardCustom.classList.add("hide");

        const cardFooter = document.createElement('div');
        cardFooter.classList.add("card-footer");        

        const priority = new Text(priorityProps).render();
        priority.textContent = `Срочность: ${this._priority}`;        

        const status = new Text(statusProps).render();
        status.textContent = `Статус: ${this._status}`;

        const title = new Text(titleProps).render();
        title.textContent = this._title;     
        
        const description = new Text(descriptionProps).render();
        description.textContent = this._description;
      
        const patient = new Text(patientProps).render();
        patient.textContent = `Пациент: ${this._patient}`;

        const doctor = new Text(doctorProps).render();
        doctor.textContent = `Доктор: ${this._doctor}`;
        
        const comments = new Text(commentsProps).render();
        comments.textContent = `Комментарии: ${this._comments}`;

        const showMoreButton = new Button(showMoreProps).render();
        showMoreButton.addEventListener("click", (e) => {
            this.handleShowMore(e, cardCustom);
        });       

        const editSelect = new Select(editProps).render();
        editSelect.addEventListener("click", async (e) => {
            if (e.target.value == "edit") {
                new EditVisitModal().render(this);
            }
            else if (e.target.value == "delete") {
                const status = await deleteVisit(this._id);
                if (status.toString() === "200") {
                    document.getElementById("visits").removeChild(this._cardElement);
                }
            }
        });

        cardHeader.append(priority);
        cardHeader.append(status);
        cardBody.append(title);
        cardBody.append(description);
        cardBody.append(patient);
        cardBody.append(doctor); 
        cardCustom.append(comments);     
        cardFooter.append(showMoreButton);
        cardFooter.append(editSelect);
        card.append(cardHeader);
        card.append(cardBody);
        card.append(cardCustom);
        card.append(cardFooter);

        this._cardElement = card;
        return card;
    };
} 