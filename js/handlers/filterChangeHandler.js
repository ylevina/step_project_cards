import { getAllVisits } from "../fetch.js";
import { default as visitFactory } from "../visits/visitFactory.js";

export async function filterChangeHandler() {
    const searchPhrase = document.querySelector("#search").value.toLowerCase();
    const status = document.querySelector("#status").value.toLowerCase();
    const priority = document.querySelector("#priority").value.toLowerCase();
   
    let visits = await getAllVisits();
    visits = visits.map(visit => visitFactory(visit));
    visits = visits.filter((visit) => typeof visit !== 'undefined');


    let filteredVisits = visits.filter(visit => visit._title.toLowerCase().includes(searchPhrase) || visit._description.toLowerCase().includes(searchPhrase));
    if (status !== "все") filteredVisits = filteredVisits.filter(visit => visit._status.toLowerCase() === status);
    if (priority !== "все") filteredVisits = filteredVisits.filter(visit => visit._priority.toLowerCase() === priority);
    
    window.visits = filteredVisits;

    try {
        const visitsArea = document.querySelector("#visits");        
        visitsArea.innerHTML = "";
        if(window.visits.length == 0) visitsArea.textContent = "По заданному фильтру ничего не найдено.";
        window.visits.forEach(visit => visitsArea.append(visit.render()));

    } catch (e) {
        console.error(e);
    }
}