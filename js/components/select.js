import { default as Component } from "./component.js";

export default class Select extends Component {

    render() {
        const { options, className, id = '', name = '' } = this.props;

        const element = this.createElement("select", { className: className, id: id, name: name }, '', options);
        this.elem = element;
        return element;
    }
}
