import Component from "./component.js";

export default class Button extends Component {
 
  render() {
    const { ...attr } = this.props;
    const element = this.createElement("button", attr);
    this.elem = element;
    return element;
  }
}
