import { Modal } from "./modal.js";
import { default as EditVisitForm } from "../forms/editVisitForm.js";

export class EditVisitModal extends Modal {

    render(visit) {
        const MODAL_ID = 'editVisitModal';
        let modal = null;
        if(super.exists(MODAL_ID)) {
            modal = document.getElementById(MODAL_ID);
        } else {
            modal = super.render(MODAL_ID);
            let objVisitForm = new EditVisitForm();

            let closeSpan = document.createElement('span');
            closeSpan.className = 'close';
            closeSpan.innerHTML = '&times;';

            let title = document.createElement('p');
            title.innerHTML = 'Редактировать визит';
            let doctorSelect = objVisitForm.renderSelect();
            modal.querySelector(".modal-content").append(
                closeSpan, title, doctorSelect);

            document.body.append(modal);
            closeSpan.addEventListener('click', () => {
                super.closeModal(modal);
                visit._cardElement.querySelector('#edit').selectedIndex = 0;
            });

            window.addEventListener('click', (e) => {
                if (e.target == modal) {
                    super.closeModal(modal);
                    visit._cardElement.querySelector('#edit').selectedIndex = 0;
                }
            })

            const clone = Object.assign({}, visit);
            objVisitForm.chooseDoctor(doctorSelect, clone);
        }      

        super.openModal(modal);      
    }    
}
