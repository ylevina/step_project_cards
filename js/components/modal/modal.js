export class Modal {

    constructor(name, btnOpener, btnClose) {
        this.name = name;
        this.btnOpener = btnOpener;
        this.btnClose = btnClose;
    }

    init() {
        const modal = document.getElementById(this.name);
        const btnOpener = document.getElementById(this.btnOpener);
        const btnClose = document.getElementsByClassName(this.btnClose)[0];
        const $this = this; // class Modal

        btnOpener.onclick = function () {
            this.openModal(modal);
        }

        btnClose.onclick = function () {
            this.closeModal(modal);
        }

        window.onclick = function (event) {
            if (event.target == modal) {
                this.closeModal(modal);
            }
        }
    }

    render(id) {
        let divModal = document.createElement("div");
        divModal.className = 'modal';
        divModal.id = id;
        let divModalContent = document.createElement("div");
        divModalContent.className = 'modal-content';
        divModal.append(divModalContent);
        return divModal;
    }
    
    openModal(modal) {
        modal.style.display = "block";
    }

    closeModal(modal) {
        document.body.removeChild(modal);
    }

    exists(id) {
        return (document.getElementById(id) ? true : false);     
    }
}