import { Modal } from "./modal.js";
import { default as VisitForm } from "../forms/visitForm.js";

export class DoctorModal extends Modal {

    render() {
        const MODAL_ID = 'doctorModal';
        let modal = null;
        if(super.exists(MODAL_ID)) {
            modal = document.getElementById(MODAL_ID);
        } else {
            modal = super.render(MODAL_ID);
            let objVisitForm = new VisitForm();

            let closeSpan = document.createElement('span');
            closeSpan.className = 'close';
            closeSpan.innerHTML = '&times;';

            let visit = document.createElement('p');
            visit.innerHTML = 'Создать визит';
            let form = objVisitForm.renderSelect();
            modal.querySelector(".modal-content").append(
                closeSpan, visit, form);

            document.body.append(modal);
            closeSpan.addEventListener('click', () => {
                super.closeModal(modal);
            });

            window.addEventListener('click', (e) => {
                if (e.target == modal) {
                    super.closeModal(modal);
                }
            })

            objVisitForm.chooseDoctor(form);
        }

        super.openModal(modal);

    }
}


