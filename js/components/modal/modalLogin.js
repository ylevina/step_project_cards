import { Modal } from "./modal.js";
import { default as LoginForm } from "../forms/loginForm.js";

export class LoginModal extends Modal {

    render() {
        const MODAL_ID = 'loginModal';
        let modal = null;
        if(super.exists(MODAL_ID)) {
            modal = document.getElementById(MODAL_ID);
        } else {
            modal = super.render(MODAL_ID);
            let objLoginForm = new LoginForm();

            let closeSpan = document.createElement('span');
            closeSpan.className = 'close';
            closeSpan.innerHTML = '&times;';

            let login = document.createElement('p');
            login.innerHTML = 'Login';
            let form = objLoginForm.render();
            console.log(form);
            modal.querySelector(".modal-content").append(
                closeSpan, login, form);

            document.body.append(modal);
            closeSpan.addEventListener('click', () => {
                super.closeModal(modal);
            });

            window.addEventListener('click', (e) => {
                if (e.target == modal) {
                    super.closeModal(modal);
                }
            })
        }

        super.openModal(modal);

    }
}


