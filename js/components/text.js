import Component from "./component.js";

export default class Text extends Component {
 
  render() {
    const { ...attr } = this.props;
    const element = this.createElement("p", attr);
    this.elem = element;
    return element;
  }
}
