export default class Component {
    constructor(props) {
        this.props = { ...props };
    }

    createElement(tag, attr, content = '', options='') {
        const element = document.createElement(tag);
        for (const [key, value] of Object.entries(attr)) {
           
            if (value) {
                element[key] = value;
            }
        }

        if (options) {
            options.forEach(option => {
                let optionDom = document.createElement('option');
                for (const [key, value] of Object.entries(option)) {
                    if (value) {
                        optionDom[key] = value;
                    }
                }
                element.add(optionDom);
            });
        }
        if (content) {
            element.innerHTML = content;
        }

        return element;
    }
}

