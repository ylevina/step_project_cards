import { default as Input } from "../input.js";
//import { default as VisitFormCardiolog } from "./FormCardiolog.js";
// import { default as VisitFormTherapist } from './VisitFormTherapist.js';
// import { default as VisitFormDentist } from './VisitFormDentist.js';
import { default as Select} from "../select.js";
import { default as Form } from "./form.js";

export default class VisitForm extends Form {

    listDoctor = {
        options: [
            {text:'Выберите доктора'},
            {value:'Cardiologist',text:'Кардиолог'},
            {value:'Dentist',text:'Стоматолог'},
            {value:'Therapist',text:'Терапевт'},
        ],
        name: 'doctor',
        id: 'doctor',
        className: "form-control"
    };
    
    titleProps = {
        type: "text",
        name: "title",
        id: "title",
        required: true,
        className: "form-props",
        placeholder: "Введите цель визита",
        errorText: "Ведите цель визита!"
    };

    descriptionProps = {
        type: "text",
        name: "description",
        id: "description",
        required: true,
        className: "form-props",
        placeholder: "Введите описание визита",
        errorText: "Введите описание визита!"
    };

    priorityProps = {
        options: [
                 {text:'Срочность'},
                {value:'низкая',text:'Низкая'},
                {value:'средняя',text:'Средняя'},
                {value:'высокая',text:'Высокая'}
            ],
        name: 'priority',
        id: 'priorityProp',
        className: "form-props",
        required: true,
    };

    patientProps = {
        type: "text",
        name: "patient",
        required: true,
        className: "form-props",
        id: 'patient',
        placeholder: "ФИО",
        errorText: "Введите ФИО"
    };

    commentsProps = {
        type: "text",
        name: "comments",
        id: "comments",
        required: false,
        className: "form-props",
        placeholder: "Камменты",
    };

    statusProps = {
        type: "hidden",
        value: "новый",
        name: "status",
        id: "statusProp",
    };

    btnSubmit = {
        value: "Создать",
        type: "submit",
        name: "submit",
        className: "form-props"
    };

    renderSelect() {
        const listDoctorsSelect = new Select(this.listDoctor);
        const form = super.render(this.props);
        form.append(listDoctorsSelect.render());
        return form;
    }

    refresh(){
        const refreshingProps = document.querySelectorAll('.form-props');
        
        refreshingProps.forEach(item => {
            item.remove();
        })
    }
    
    chooseDoctor(form){
      
        const selectDoctor = document.querySelector('#doctor');
        
        selectDoctor.addEventListener('change', (e) => {
            this.refresh();
            if (e.target.value === 'Cardiologist') {
                const cardiologForm = new VisitFormCardiolog();
                cardiologForm.render(form);
            }  if (e.target.value === 'Dentist') {
                const dentistForm = new VisitFormDentist();
                return dentistForm.render(form);
            } else if (e.target.value === 'Therapist') 
            {   
                const therapistForm = new VisitFormTherapist();
                return therapistForm.render(form);
            }
        })       
    }

    render(form) {
        const {
            statusProps,
            titleProps,
            descriptionProps,
            priorityProps,
            patientProps,
            commentsProps,
            btnSubmit
        } = this;
        
    
        const titleInput = new Input(titleProps);
        const descriptionInput = new Input(descriptionProps);
        const prioritySelect = new Select(priorityProps);
        const patientInput = new Input(patientProps);
        const commentsInput = new Input(commentsProps);
        const statusInput =  new Input(statusProps);
        const btn = new Input(btnSubmit);

        if(form){
            form.append(
                statusInput.render(),
                titleInput.render(),
                descriptionInput.render(),
                prioritySelect.render(),
                patientInput.render(),
                commentsInput.render(),
                btn.render()
            );
    
            return form;
        }
    }        
}

class VisitFormCardiolog extends VisitForm {

    bpProps = {
        type: "text",
        name: "bp",
        id: "bp",
        required: true,
        className: "form-props",
        placeholder: "Обычное давление",
        errorText: "Укажите давление!"
    };

    bmiProps = {
        type: "text",
        name: "bmi",
        id: "bmi",
        required: true,
        className: "form-props",
        placeholder: "Индекс массы тела",
        errorText: "Введите индекс массы тела!"
    }; 

    deseasesProps = {
        type: "text",
        name: "deseases",
        id: "deseases",
        required: true,
        className: "form-props",
        placeholder: "Перенесенные заболевания сердечно-сосудистой системы",
        errorText: "Укажите заболевания!"
    };

    ageProps = {
        type: "text",
        name: "age",
        id: "age",
        required: true,
        className: "form-props",
        placeholder: "Возраст",
        errorText: "Укажите возраст!"
    };

    render(form) {

        super.render(form);

        const bpProps = new Input(this.bpProps);
        const lastElement = form.querySelector('#patient');
        lastElement.before(bpProps.render());
        
        const bmiProps = new Input(this.bmiProps);
        const bp = form.querySelector('#bp');
        bp.after(bmiProps.render());
        
        const deseasesProps = new Input(this.deseasesProps);
        const bmi = form.querySelector('#bmi');
        bmi.after(deseasesProps.render())
    }
}

class VisitFormDentist extends VisitForm {

    lastVisitDateProps = {
        type: "text",
        name: "lastVisitDate",
        id: "lastVisitDate",
        required: true,
        className: "form-props",
        placeholder: "Дата последнего посещения",
        errorText: "Укажите дату последнего посещения!"
    };

    render(form) {
        super.render(form);
        
        const lastVisitDateProps = new Input(this.lastVisitDateProps);
        const lastElement = form.querySelector('#patient');
        lastElement.before(lastVisitDateProps.render());
        
    }

}

class VisitFormTherapist extends VisitForm {

    ageProps = {
        type: "text",
        name: "age",
        id: "age",
        required: true,
        className: "form-props",
        placeholder: "Возраст",
        errorText: "Укажите возраст!"
    };

    render(form) {
        super.render(form);
        
        const ageProps = new Input(this.ageProps);
        const lastElement = form.querySelector('#patient');
        lastElement.before(ageProps.render());
    }

}