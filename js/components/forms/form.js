import { default as Component } from "../component.js";
import visitFactory from "../../visits/visitFactory.js";

export default class Form extends Component {

    handleSubmit = async function (data) {
        const url = 'http://cards.danit.com.ua/cards';
        
        try {
            const token = localStorage.getItem('token');
            const response = await fetch(url, {
                method: 'POST',
                body: JSON.stringify(data),
                headers: {
                    Authorization: `Bearer ${token}`, 
                    'Content-Type': 'application/json'
                }
            });
            const json = await response.json();
            console.log("json:",json);

            const modal = document.getElementById("doctorModal");
            document.body.removeChild(modal);
            const card = visitFactory(json).render();
            document.querySelector("#visits").append(card);
           
        } catch (error) {
            console.error('Error:', error);
        }
    };

    render() {
        const { ...attr } = this.props;
        const form = this.createElement("form", attr);
        this.form = form;
        form.addEventListener("submit", (e) => {

            e.preventDefault();
            const fields = Array.from(
                this.form.querySelectorAll("input[name], select[name], textarea[name]")
            );

            const data = {};

            fields.forEach(({ name, value }) => {
                data[name] = value;
            });
            console.log("data:",data);
            this.handleSubmit(data);
        });
        return this.form;
    }

    serializeJSON() {
        const fields = Array.from(
            this.form.querySelectorAll("input[name], select[name], textarea[name]")
        );
        const data = {};
        fields.forEach(({ name, value }) => { data[name] = value; });
        return JSON.stringify(data);
    }
}