import { default as Input } from "../input.js";
import { default as Form } from "./form.js";
import { default as Select } from "../select.js";
import visitFactory from "../../visits/visitFactory.js";

export default class EditVisitForm extends Form {

    visitId = 0;

    handleSubmit = async function (data) {
        const url = `http://cards.danit.com.ua/cards/${this.visitId}`;
        console.log("data:", data);        

        try {
            const token = localStorage.getItem('token');
            const response = await fetch(url, {
                method: 'PUT',
                body: JSON.stringify(data),
                headers: {
                    Authorization: `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            });
            const json = await response.json();

            const modal = document.getElementById("editVisitModal");
            document.body.removeChild(modal);
            const card = visitFactory(json).render();
            document.querySelector("#visits").append(card);

        } catch (error) {
            console.error('Error:', error);
        }
    };

    listDoctor = {
        options: [
            { text: 'Выберите доктора' },
            { value: 'Cardiologist', text: 'Кардиолог' },
            { value: 'Dentist', text: 'Стоматолог' },
            { value: 'Therapist', text: 'Терапевт' },
        ],
        name: 'doctor',
        id: 'edit-doctor',
        className: "form-control"
    };

    titleProps = {
        type: "text",
        name: "title",
        id: "title",
        required: true,
        className: "form-props",
        placeholder: "Введите цель визита",
        errorText: "Ведите цель визита!"
    };

    descriptionProps = {
        type: "text",
        name: "description",
        id: "description",
        required: true,
        className: "form-props",
        placeholder: "Введите описание визита",
        errorText: "Введите описание визита!"
    };

    priorityProps = {
        options: [
            { text: 'срочность' },
            { value: 'низкая', text: 'низкая' },
            { value: 'средняя', text: 'средняя' },
            { value: 'высокая', text: 'высокая' }
        ],
        name: 'priority',
        id: 'priorityProp',
        className: "form-props",
        required: true,
    };

    patientProps = {
        type: "text",
        name: "patient",
        required: true,
        className: "form-props",
        id: 'patient',
        placeholder: "ФИО",
        errorText: "Введите ФИО"
    };

    commentsProps = {
        type: "text",
        name: "comments",
        id: "comments",
        required: false,
        className: "form-props",
        placeholder: "Комментарии",
    };

    statusProps = {
        type: "hidden",
        value: "новый",
        name: "status",
        id: "statusProp",
    };

    btnSubmit = {
        value: "Сохранить",
        type: "submit",
        name: "submit",
        className: "form-props"
    };

    renderSelect() {
        const listDoctorsSelect = new Select(this.listDoctor);
        const form = super.render(this.props);
        form.append(listDoctorsSelect.render());
        return form;
    }

    refresh() {
        const refreshingProps = document.querySelectorAll('.form-props');

        refreshingProps.forEach(item => {
            item.remove();
        })
    }

    chooseDoctor(form, visit) {
        this.visitId = visit._id;    

        const selectDoctor = document.querySelector('#edit-doctor');

        selectDoctor.addEventListener('change', (e) => {
            this.refresh();
            if (e.target.value === 'Cardiologist') {
                visit._doctor = 'Cardiologist';
                const cardiologForm = new VisitFormCardiolog();
                return cardiologForm.render(form, visit);
            } if (e.target.value === 'Dentist') {
                visit._doctor = 'Dentist';
                const dentistForm = new VisitFormDentist();
                return dentistForm.render(form, visit);
            } else if (e.target.value === 'Therapist') {
                visit._doctor = 'Therapist';
                const therapistForm = new VisitFormTherapist();
                return therapistForm.render(form, visit);
            }
        })

        if (visit._doctor === 'Cardiologist') {
            const cardiologForm = new VisitFormCardiolog();
            return cardiologForm.render(form, visit);
        } else if (visit._doctor === 'Dentist') {
            const dentistForm = new VisitFormDentist();
            return dentistForm.render(form, visit);
        } else if (visit._doctor === 'Therapist') {
            const therapistForm = new VisitFormTherapist();
            return therapistForm.render(form, visit);
        }
    }

    render(form) {
        const {
            statusProps,
            titleProps,
            descriptionProps,
            priorityProps,
            patientProps,
            commentsProps,
            btnSubmit
        } = this;


        const titleInput = new Input(titleProps);
        const descriptionInput = new Input(descriptionProps);
        const prioritySelect = new Select(priorityProps);
        const patientInput = new Input(patientProps);
        const commentsInput = new Input(commentsProps);
        const statusInput = new Input(statusProps);
        const btn = new Input(btnSubmit);

        if (form) {
            form.append(
                statusInput.render(),
                titleInput.render(),
                descriptionInput.render(),
                prioritySelect.render(),
                patientInput.render(),
                commentsInput.render(),
                btn.render()
            );

            return form;
        }
    }

    prefill(visit) {
        const {
            titleProps,
            descriptionProps,
            priorityProps,
            patientProps,
            commentsProps
        } = this;
      
        document.querySelector(`[name='doctor']`).value = visit._doctor;
        document.querySelector(`[name='${titleProps.name}']`).value = visit._title;
        document.querySelector(`[name='${descriptionProps.name}']`).value = visit._description;
        document.querySelector(`[name='${patientProps.name}']`).value = visit._patient;
        document.querySelector(`[name='${priorityProps.name}']`).value = visit._priority;
        if(visit._comments) document.querySelector(`[name='${commentsProps.name}']`).value = visit._comments;
    }
}

class VisitFormCardiolog extends EditVisitForm {

    bpProps = {
        type: "text",
        name: "bp",
        id: "bp",
        required: true,
        className: "form-props",
        placeholder: "Обычное давление",
        errorText: "Укажите давление!"
    };

    bmiProps = {
        type: "text",
        name: "bmi",
        id: "bmi",
        required: true,
        className: "form-props",
        placeholder: "Индекс массы тела",
        errorText: "Введите индекс массы тела!"
    };

    deseasesProps = {
        type: "text",
        name: "deseases",
        id: "deseases",
        required: true,
        className: "form-props",
        placeholder: "Перенесенные заболевания сердечно-сосудистой системы",
        errorText: "Укажите заболевания!"
    };

    ageProps = {
        type: "text",
        name: "age",
        id: "age",
        required: true,
        className: "form-props",
        placeholder: "Возраст",
        errorText: "Укажите возраст!"
    };

    prefill(visit) {
        super.prefill(visit);
        const {
            bpProps,
            bmiProps,
            deseasesProps,
        } = this;

        if(visit._bp) document.querySelector(`[name='${bpProps.name}']`).value = visit._bp;
        if(visit._bmi) document.querySelector(`[name='${bmiProps.name}']`).value = visit._bmi;
        if(visit._deseases) document.querySelector(`[name='${deseasesProps.name}']`).value = visit._deseases;
    }

    render(form, visit) {

        super.render(form, visit);

        const bpProps = new Input(this.bpProps);
        const lastElement = form.querySelector('#patient');
        lastElement.before(bpProps.render());

        const bmiProps = new Input(this.bmiProps);
        const bp = form.querySelector('#bp');
        bp.after(bmiProps.render());

        const deseasesProps = new Input(this.deseasesProps);
        const bmi = form.querySelector('#bmi');
        bmi.after(deseasesProps.render());
        this.prefill(visit);
    }
}

class VisitFormDentist extends EditVisitForm {

    lastVisitDateProps = {
        type: "text",
        name: "lastVisitDate",
        id: "lastVisitDate",
        required: true,
        className: "form-props",
        placeholder: "Дата последнего посещения",
        errorText: "Укажите дату последнего посещения!"
    };

    prefill(visit) {
        super.prefill(visit);

        const {
            lastVisitDateProps
        } = this;

        if(visit._lastVisitDate) document.querySelector(`[name='${lastVisitDateProps.name}']`).value = visit._lastVisitDate;
    }

    render(form, visit) {
        super.render(form);

        const lastVisitDateProps = new Input(this.lastVisitDateProps);
        const lastElement = form.querySelector('#patient');
        lastElement.before(lastVisitDateProps.render());
        this.prefill(visit);
    }
}

class VisitFormTherapist extends EditVisitForm {

    ageProps = {
        type: "text",
        name: "age",
        id: "age",
        required: true,
        className: "form-props",
        placeholder: "Возраст",
        errorText: "Укажите возраст!"
    };

    prefill(visit) {
        super.prefill(visit);

        const {
            ageProps
        } = this;

        if(visit._age) document.querySelector(`[name='${ageProps.name}']`).value = visit._age;
    }

    render(form, visit) {
        super.render(form);

        const ageProps = new Input(this.ageProps);
        const lastElement = form.querySelector('#patient');
        lastElement.before(ageProps.render());
        this.prefill(visit);
    }
}