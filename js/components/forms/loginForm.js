import { default as Input } from "../input.js";
import { default as Button } from "../button.js";
import { default as Form } from "./form.js";

export default class LoginForm extends Form {
    handleSubmit = async function (data) {
        const url = 'http://cards.danit.com.ua/login';

        try {
            const response = await fetch(url, {
                method: 'POST',
                body: this.serializeJSON(),              
                headers: {
                    'Accept': 'application/json, text/plain, */*',
                    'Content-Type': 'application/x-www-form-urlencoded'}              
            });
            const json = await response.json();

            if(json.status != "Error"){
                localStorage.setItem('token', json.token); 
                document.location.reload(true);  
            }
            else {
                alert ("Ошибка авторизации - введен неверный e-mail или пароль");
            }
        } catch (error) {
            console.error('Error:', error);
            alert("Ошибка подключения");
        }
    };

    emailProps = {
        type: "text",
        name: "email",
        id: "email",
        required: true,
        className: "form-props",
        placeholder: "Введите email",
        // errorText: "Введите email!"
    };

    passwordProps = {
        type: "password",
        name: "password",
        id: "password",
        required: true,
        className: "form-props",
        placeholder: "Введите пароль",
        errorText: "Введите пароль!"
    };

    btnSubmit = {
        textContent: "Войти",
        type: "submit",
        name: "submit",
        className: "form-props"
    };

        btnPassHider = {
        id: "eye",
        type: "button",
        className: "passwordHider"
    };

    render() {        
        const { emailProps, passwordProps, btnSubmit, btnPassHider } = this;
        const form = super.render(this.props);  

        const emailInput = new Input(emailProps);
        const passwordInput = new Input(passwordProps);
        const btn = new Button(btnSubmit);
        const btnPass = new Button(btnPassHider);

        form.append(
            emailInput.render(),
            passwordInput.render(),
            btn.render(),
            btnPass.render()
        );
        return form;
    }
}
