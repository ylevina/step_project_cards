import { default as Input } from "../input.js";
import { default as Form } from "./form.js";
import { filterChangeHandler } from "../../handlers/filterChangeHandler.js";
import Select from "../select.js";

export default class FilterForm extends Form {
  searchProps = {
    type: "text",
    name: "email",
    required: true,    
    placeholder: "Искать в названии или описании",
    id: 'search',
    className: "form-control"    
  };  

  statusSelectProps = {
    options: [
        {value:"все",text:'Статус'},
        {value:'новый',text:'Новые'},
        {value:'завершен',text:'Завершенные'},
    ],
    id: 'status',
    className: "form-control"
};

prioritySelectProps = {
  options: [
      {value:"все",text:'Срочность'},
      {value:'низкая',text:'Низкая'},
      {value:'средняя',text:'Средняя'},
      {value:'высокая',text:'Высокая'},
  ],
  id: 'priority',
  className: "form-control"
};

  handleSubmit = () => {   
  };

  handleOnChange = () => { filterChangeHandler() };

  render() {
    const { searchProps, statusSelectProps, prioritySelectProps } = this;
    const searchInput = new Input(searchProps);
    const statusSelect = new Select(statusSelectProps);
    const prioritySelect = new Select(prioritySelectProps);
    const form = super.render(this.props);   
    form.append(
      searchInput.render(),
      statusSelect.render(),
      prioritySelect.render(),
    );
    form.addEventListener("change", () => { 
      this.handleOnChange()});
    return form;
  }
}
