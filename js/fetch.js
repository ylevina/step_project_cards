const BASE_URL = "https://cards.danit.com.ua";

export async function login() {
    const response = await fetch(`${BASE_URL}/login`, {
        method: "POST",
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: JSON.stringify({ "email": "dan.test@gmail.com", "password": "password" })
    });

    return await response.json();
}

export async function getAllVisits() {
    const response = await fetch(`${BASE_URL}/cards`, {
        method: "GET",
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('token')}`,
        }
    });

    return await response.json();
}

export async function getVisit(id) {
    const response = await fetch(`${BASE_URL}/cards/${id}`, {
        method: "GET",
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('token')}`,
        }
    });
    const card = await response.json();
    return card;
}

export async function deleteVisit(id) {
    const response = await fetch(`${BASE_URL}/cards/${id}`, {
        method: "DELETE",
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('token')}`,
        }
    });
    
    return await response.status;
}