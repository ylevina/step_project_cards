import { getAllVisits } from "./fetch.js";
import { default as visitFactory } from "./visits/visitFactory.js";

export async function applyFilter() {
    const searchPhrase = document.querySelector("#search").value.toLowerCase();
    const status = document.querySelector("#status").value.toLowerCase();
    const priority = document.querySelector("#priority").value.toLowerCase();
   
    const cards = await getAllVisits();
    let filteredVisits = cards.filter(card => card.title.toLowerCase().includes(searchPhrase) || card.description.toLowerCase().includes(searchPhrase));
    if (status !== "все") filteredVisits = filteredVisits.filter(card => card.status.toLowerCase() === status);
    if (priority !== "все") filteredVisits = filteredVisits.filter(card => card.priority.toLowerCase() === priority);

    //ToDo: move to separate function and file
    try {
        filteredVisits = filteredVisits.map(card => visitFactory(card));
        const visitsArea = document.querySelector(".visits");        
        visitsArea.innerHTML = "";
        if(filteredVisits.length == 0) visitsArea.textContent = "По заданному фильтру ничего не найдено.";
        filteredVisits.forEach(visit => visitsArea.append(visit.render()));

    } catch (e) {
        console.error(e);
    }
}