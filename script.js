import { getAllVisits } from "./js/fetch.js";
import { default as visitFactory } from "./js/visits/visitFactory.js";
import { DoctorModal } from './js/components/modal/modalDoctor.js';
import { LoginModal } from './js/components/modal/modalLogin.js';
import { default as Button } from "./js/components/button.js";
import FilterForm from "./js/components/forms/filterForm.js";

window.onload = async function () {

    const token = localStorage.getItem("token");

    if (token && token !== "null") {
        const createLogoutButton = new Button({
            textContent: "Выйти",
            id: "createLogout"
        }).render();
        createLogoutButton.addEventListener("click", () => {
            localStorage.removeItem('token');
            document.location.reload(true);
        });
        document.body.append(createLogoutButton);

        const createVisitButton = new Button({
            textContent: "Создать визит",
            id: "createVisit"
        }).render();
        createVisitButton.addEventListener("click", () => {
            new DoctorModal().render();
        });
        document.body.append(createVisitButton);

        const filterForm = new FilterForm({ id: "filter" }).render();
        document.body.append(filterForm);

        try {
            const visitsRes = await getAllVisits();
            
            const visitsArea = document.createElement("div");
            visitsArea.setAttribute("id", "visits")

            if (visitsRes && visitsRes.length > 0) {
                let visits = visitsRes.map((visit) => visitFactory(visit));
                visits = visits.filter((visit) => typeof visit !== 'undefined');
                visits.forEach(visit => visitsArea.append(visit.render()));
            }
            else {
                const noItemsMessageElement = document.createElement('p');
                noItemsMessageElement.textContent = "Ничего не добавлено.";
                visitsArea.append(noItemsMessageElement);
            }

            document.body.append(visitsArea);

        } catch (e) {
            console.error(e);
        }
    }
    else {
        const loginButton = new Button({
            textContent: "Вход",
            id: "login"
        }).render();
        loginButton.addEventListener("click", () => {
            let modalLogin = new LoginModal().render();
            const passHiderButton = document.getElementById('eye');
            passHiderButton.addEventListener('click', hide, false);
            function hide() 
            {
                if (password.type == 'text') { 
                        password.type = 'password'; 
                        passHiderButton.classList.remove('view');}
                else { password.type = 'text'; 
                        passHiderButton.classList.add('view'); };
            }
        });
        document.body.append(loginButton);
    }
};