# Step Project Cards

## Participants: 
- Kateryna Sosnikova 
- Roman Semywolos
- Yuliia Levina

### Features done by Roman Semywolos: 
- login/authorization
- logout

### Features done by Kateryna Sosnikova: 
- modal
- visit forms
- create visit

### Features done by Yuliia Levina: 
- filter
- cards/visits display
- edit card/delete card